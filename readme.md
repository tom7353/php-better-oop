# PHP BetterOOP

A set of utility classes that provide you with common functions like other programming languages do.

You know for sure how hard PHP can be because it evolved over the years. No consistent naming convention and sometimes
it's hard to read. Take this example:
```php
$string = trim(str_replace(["Hallo", "Welt"],
                           ["Hello", "World"],
                           substr("Hallo Welt!", 0, 10));
echo $string;
```
It is hard to read, isn't it? And all it does is simply translate "Hallo Welt!" to "Hello World"... *And I know, that
this example may not be an appropriate read world situation, but we have all been there: Chaining methods which simply
should not be chained.*

Now, let's see what we could do better:
```php
$charSequence = (new CharSequence("Hallo Welt"))
  ->substring(0, 10)
  ->replace("Hallo", "Hello")
  ->replace("Welt", "World")
  ->trim();
echo $charSequence;
```
Isn't that much more clean and programmer-friendly?

Of course you'll always have a little runtime overhead, but if you're interested in great performance, may I ask why you
are using PHP? Great, I don't think you have an answer – so, to start just run
`composer require tomjanke/php-better-oop`!

## Installation

Well, if you're using composer, it's just a single command:
```bash
composer require tomjanke/php-better-oop
```

If not, I recommend you using it. It's a great tool to manage your own sources and also it's the only really good way to
use this library for now.

## Content

### `BetterOOP\Collection\Collection`

An ordered, type-safe set of elements which can easily be modified, filtered or reordered.

### `BetterOOP\Map\Map`

An ordered, type-safe list of key-value pairs which can easily be modified.

### `BetterOOP\String\CharSequence`

A wrapper for a default string, which provides useful string manipulation methods.