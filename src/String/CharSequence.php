<?php
/**
 * MIT License
 *
 * Copyright (c) 2019 Tom Janke
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace BetterOOP\String;


use ArrayIterator;
use InvalidArgumentException;
use IteratorAggregate;
use JsonSerializable;
use Serializable;

/**
 * A functional implementation for strings.
 *
 * @package BetterOOP\String
 * @author Tom Janke <info@tomjanke.de>
 */
class CharSequence implements IteratorAggregate, JsonSerializable, Serializable {

  /** @var string */
  private $string;

  /**
   * Constructs a new char sequence based on the given string.
   * @param string $string
   */
  public function __construct(string $string) {
    $this->string = $string;
  }

  /**
   * Returns the character at the specified index.
   *
   * @param int $index
   * @return string
   */
  public function charAt(int $index): string {
    if ($index < 0 || $index >= $this->length())
      throw new InvalidArgumentException("Index cannot be negative, equal to or greater than length.");

    return substr($this->string, $index, 1);
  }

  /**
   * Checks whether the subject is equals to the string in a case sensitive way.
   *
   * @param string $other
   * @return bool
   */
  public function equals(string $other): bool {
    return strcmp($this->string, $other) === 0;
  }

  /**
   * Checks whether the subject is equals to the string in a case insensitive way.
   *
   * @param string $other
   * @return bool
   */
  public function equalsIgnoreCase(string $other): bool {
    return strcmp(mb_strtolower($this->string), mb_strtolower($other)) === 0;
  }

  /**
   * Checks whether the string ends with the given subject.
   *
   * @param string $suffix
   * @return bool
   */
  public function endsWith(string $suffix): bool {
    return mb_strpos($this->string, $suffix) === $this->length() - mb_strlen($suffix);
  }

  /**
   * Checks whether the string starts with the given subject.
   *
   * @param string $prefix
   * @return bool
   */
  public function startsWith(string $prefix): bool {
    return mb_strpos($this->string, $prefix) === 0;
  }

  /**
   * Returns the length of this string.
   *
   * @return int
   */
  public function length(): int {
    return mb_strlen($this->string);
  }

  /**
   * Replaces the specified search value with the replacement value if found.
   *
   * @param string $search
   * @param string $replace
   * @return CharSequence
   */
  public function replace(string $search, string $replace): self {
    $this->string = str_replace($search, $replace, $this->string);
    return $this;
  }

  /**
   * Splits this sequence at the specified delimiter.
   *
   * @param string $at
   * @param int|null $limit
   * @return array
   */
  public function split(string $at, ?int $limit = null): array {
    return explode($at, $this->string, $limit ?? PHP_INT_MAX);
  }

  /**
   * Creates a substring by using the specified start and optional end index.
   *
   * @param int $beginIndex
   * @param int|null $endIndex
   * @return $this
   */
  public function cut(int $beginIndex, ?int $endIndex = null) {
    $this->string = mb_substr($this->string, $beginIndex, $endIndex === null ? null : $endIndex - $beginIndex + 1);
    return $this;
  }

  /**
   * Creates a new sequence and returns a substring by using the specified start and optional end index.
   *
   * @param int $beginIndex
   * @param int|null $endIndex
   * @return CharSequence
   */
  public function substring(int $beginIndex, ?int $endIndex = null): self {
    return $this->copy()->cut($beginIndex, $endIndex);
  }

  /**
   * Converts this sequence to lowercase
   *
   * @return CharSequence
   */
  public function toLowerCase(): self {
    $this->string = mb_strtolower($this->string);
    return $this;
  }

  /**
   * Converts this sequence to uppercase
   *
   * @return CharSequence
   */
  public function toUpperCase(): self {
    $this->string = mb_strtoupper($this->string);
    return $this;
  }

  /**
   * Removes all surrounding whitespace, see {@link trim}
   *
   * @return CharSequence
   */
  public function trim(): self {
    $this->string = trim($this->string);
    return $this;
  }

  /**
   * Duplicates the char sequence and returns the new char sequence.
   *
   * @return CharSequence An identical copy of the char sequence.
   */
  public function copy(): CharSequence {
    return new self($this);
  }

  /**
   * Appends to the end of the string
   *
   * @param string $suffix
   * @return CharSequence
   */
  public function append(string $suffix): self {
    $this->string .= $suffix;
    return $this;
  }

  /**
   * Prepends to the start of the string
   *
   * @param string $prefix
   * @return CharSequence
   */
  public function prepend(string $prefix): self {
    $this->string = $prefix.$this->string;
    return $this;
  }

  /**
   * Matches the string against the given regex.
   *
   * @param string $regex
   * @return bool
   */
  public function matches(string $regex): bool {
    return preg_match($regex, $this->string) === 1;
  }

  /**
   * Replaces matched parts of this sequence by the given string.
   *
   * @param string $regex
   * @param string $replacement
   * @return CharSequence
   */
  public function replaceRegEx(string $regex, string $replacement): CharSequence {
    $this->string = preg_replace($regex, $replacement, $this->string);
    return $this;
  }

  /**
   * <tt>clone $charSequence</tt>
   */
  public function __clone() {
    return $this->copy();
  }

  /**
   * <tt>echo $charSequence</tt>
   * @return string
   */
  public function __toString() {
    return $this->string;
  }

  /**
   * Creates a char sequence based on the given numbers which are converted from ascii.
   *
   * @param array $data
   * @return CharSequence
   */
  public static function fromAsciiArray(array $data): self {
    $data = array_map(function ($a) {
      return chr($a);
    }, $data);
    return new self(implode("", $data));
  }

  /**
   * Creates a char sequence based on the given characters (or strings) in the array.
   *
   * @param array $data
   * @return CharSequence
   */
  public static function fromArray(array $data): self {
    return new self(implode("", $data));
  }

  /**
   * <tt>foreach ($charSequence as $character) {...}</tt>
   * @return ArrayIterator|\Traversable
   */
  public function getIterator() {
    return new ArrayIterator(str_split($this->string));
  }

  /**
   * <tt>json_encode($charSequence)</tt>
   * @return array|mixed
   */
  public function jsonSerialize() {
    return $this->string;
  }

  /**
   * <tt>serialize($charSequence)</tt>
   * @return string
   */
  public function serialize() {
    return serialize($this->string);
  }

  /**
   * <tt>unserialize($charSequence)</tt>
   * @param string $serialized
   */
  public function unserialize($serialized) {
    $this->string = unserialize($serialized);
  }

}