<?php
/**
 * MIT License
 *
 * Copyright (c) 2019 Tom Janke
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace BetterOOP\Map;

use ArrayIterator;
use BetterOOP\Collection\Collection;
use BetterOOP\Helper\Type;
use InvalidArgumentException;
use IteratorAggregate;
use JsonSerializable;
use Serializable;

/**
 * An ordered, type-safe list of key-value pairs which can easily be modified.
 *
 * @package BetterOOP\Map
 * @author  Tom Janke <info@tomjanke.de>
 */
class Map implements IteratorAggregate, JsonSerializable, Serializable
{

  /** @var string */
  private $typeKey;

  /** @var string */
  private $typeValue;

  /** @var array */
  private $array;

  /**
   * Creates an empty map with the given types.
   *
   * @param string $typeKey
   * @param string $typeValue
   */
  public function __construct(string $typeKey, string $typeValue) {
    $this->typeKey = $typeKey;
    $this->typeValue = $typeValue;
    $this->array = [];
  }

  /**
   * Returns the values as a collection.
   *
   * @return Collection
   */
  public function values(): Collection {
    return Collection::fromArray(array_values($this->array), $this->typeValue);
  }

  /**
   * Returns the keys as a collection.
   *
   * @return Collection
   */
  public function keys(): Collection {
    return Collection::fromArray(array_keys($this->array), $this->typeKey);
  }

  /**
   * Adds one entry to the map.
   *
   * @param $key
   * @param $value
   * @return mixed|null The previous associated value or null, if no was set before
   */
  public function put($key, $value) {
    if (Type::get($key) !== $this->typeKey)
      throw new InvalidArgumentException("Invalid key type.");

    if (Type::get($value) !== $this->typeValue)
      throw new InvalidArgumentException("Invalid value type.");

    $previousValue = $this->get($key);

    $this->array[$key] = $value;

    return $previousValue;
  }

  /**
   * Adds all entries from this array to the map.
   *
   * @param array $map
   * @return Map
   */
  public function putAll(array $map): Map {
    foreach ($map as $key => $value)
      $this->put($key, $value);
    return $this;
  }

  /**
   * Receives the element at the appropriate position of the underlying array.
   *
   * @param mixed $key The index of the array
   * @return mixed|null
   */
  public function get($key) {
    if (Type::get($key) !== $this->typeKey)
      throw new InvalidArgumentException("Invalid key type.");

    if (!$this->containsKey($key))
      return null;

    return $this->array[$key];
  }

  /**
   * Removes the given key from this map.
   *
   * @param $key
   * @return Map
   */
  public function removeKey($key): Map {
    if ($this->containsKey($key))
      unset($this->array[$key]);
    return $this;
  }

  /**
   * Checks if the given element is present in this map.
   *
   * @param $element
   * @return bool
   */
  public function containsValue($element): bool {
    return array_search($element, array_values($this->array), true) !== false;
  }

  /**
   * Checks if the given key is present in this map.
   *
   * @param $key
   * @return bool
   */
  public function containsKey($key): bool {
    return array_search($key, array_keys($this->array), true) !== false;
  }

  /**
   * Checks if all the given elements are present in this map.
   *
   * @param Collection|array $elements
   * @return bool
   */
  public function containsAllValues($elements): bool {
    foreach ($elements as $element)
      if (!$this->containsValue($element))
        return false;
    return true;
  }

  /**
   * Checks if all the given keys are present in this map.
   *
   * @param Collection|array $keys
   * @return bool
   */
  public function containsAllKeys($keys): bool {
    foreach ($keys as $key)
      if (!$this->containsKey($key))
        return false;
    return true;
  }

  /**
   * Checks if this map is empty.
   *
   * @return bool
   */
  public function isEmpty(): bool {
    return $this->size() === 0;
  }

  /**
   * Returns the current count of elements in this map.
   *
   * @return int
   */
  public function size(): int {
    return sizeof($this->array);
  }

  /**
   * Duplicates the collection and returns the new map.
   *
   * @return Map An identical copy of the map.
   */
  public function copy(): Map {
    return (new Map($this->typeKey, $this->typeValue))->putAll($this->array);
  }

  /**
   * <tt>clone $map</tt>
   */
  public function __clone() {
    return $this->copy();
  }

  /**
   * <tt>echo $map</tt>
   * @return string
   */
  public function __toString() {
    $data = [];
    foreach ($this as $key => $value)
      $data[] = "{$key}=>{$value}";
    return "{$this->typeKey}=>{$this->typeValue}::[".implode(",", $data)."]";
  }

  /**
   * <tt>foreach ($map as $value) {...}</tt>
   * @return ArrayIterator|\Traversable
   */
  public function getIterator() {
    return new ArrayIterator($this->array);
  }

  /**
   * <tt>json_encode($map)</tt>
   * @return array|mixed
   */
  public function jsonSerialize() {
    return $this->array;
  }

  /**
   * <tt>serialize($map)</tt>
   * @return string
   */
  public function serialize() {
    return serialize([$this->typeKey, $this->typeValue, $this->array]);
  }

  /**
   * <tt>unserialize($map)</tt>
   * @param string $serialized
   */
  public function unserialize($serialized) {
    list($this->typeKey, $this->typeValue, $this->array) = unserialize($serialized);
  }

}