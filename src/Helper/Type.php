<?php
/**
 * MIT License
 *
 * Copyright (c) 2018 Tom Janke
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace BetterOOP\Helper;


class Type {

  public const BOOLEAN = "boolean";
  public const INTEGER = "integer";
  public const FLOAT = "double"; // yes this is intentional, see http://php.net/manual/de/function.gettype.php
  public const DOUBLE = "double";
  public const STRING = "string";
  public const ARRAY = "array";
  public const NULL = "NULL";

  /**
   * Determines the type of a given element
   *
   * @param mixed $element
   * @return string
   */
  public static function get($element): string {
    if (($type = gettype($element)) === "object")
      return get_class($element);
    return $type;
  }

}