<?php
/**
 * MIT License
 *
 * Copyright (c) 2018 Tom Janke
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace BetterOOP\Collection;

use ArrayIterator;
use BetterOOP\Helper\Type;
use InvalidArgumentException;
use IteratorAggregate;
use JsonSerializable;
use Serializable;

/**
 * An ordered, type-safe set of elements which can easily be modified, filtered or reordered.
 *
 * @package BetterOOP\Collection
 * @author  Tom Janke <info@tomjanke.de>
 */
class Collection implements IteratorAggregate, JsonSerializable, Serializable
{

  /** @var string */
  private $type;

  /** @var array */
  private $array;

  /**
   * Creates an empty collection with the given type.
   *
   * @param string $type
   */
  public function __construct(string $type) {
    $this->type = $type;
    $this->array = [];
  }

  /**
   * Adds an element to this collection at the optional specified index.
   *
   * @param mixed $element  The element to add
   * @param int|null $index The index where to add or null (a negative index counts from the end of the array whereas -1 specifies the last element)
   * @return Collection
   */
  public function add($element, ?int $index = null): self {
    if (($type = Type::get($element)) !== $this->type)
      throw new InvalidArgumentException("This collection can only take {$this->type} elements. {$type} given.");

    if ($index === null) // simply add to end
      $this->array[] = $element;

    else {
      if ($index < 0) // from the end
        $index = $this->size() + $index;

      if ($index < 0) // should not be negative now
        throw new InvalidArgumentException("Index cannot be lower than the negative size of this array.");

      if ($index >= $this->size()) // should not be greater than size
        throw new InvalidArgumentException("Index cannot be higher than the size of this array.");

      array_splice($this->array, $index, 0, [$element]);
      $this->resetKeys();
    }
    return $this;
  }

  /**
   * Searches the array for the given element and returns the index if found, null otherwise
   *
   * @param mixed $element The element to search
   * @return int|null The index or null
   */
  public function indexOf($element): ?int {
    return ($key = array_search($element, $this->array, true)) === false ? null : $key;
  }

  /**
   * Removes one (or more) elements from this collection by an index.
   *
   * @param int $index  The index to remove
   * @param int $amount Amount to remove
   * @return Collection
   */
  public function remove(int $index, int $amount = 1): self {
    if ($index >= $this->size())
      throw new InvalidArgumentException("Index cannot be higher than the actual size.");

    if ($amount < 1)
      throw new InvalidArgumentException("Amount must at least be 1.");

    if ($this->size() - $amount < $index)
      throw new InvalidArgumentException("Amount cannot be higher than size-index.");

    array_splice($this->array, $index, $amount);
    $this->resetKeys();
    return $this;
  }

  /**
   * Removes the given element from this collection.
   *
   * @param mixed $element The element to remove
   * @return Collection
   */
  public function removeElement($element): self {
    while (($index = $this->indexOf($element)) !== null) // remove all equal elements, if multiple are found
      $this->remove($index);
    return $this;
  }

  /**
   * Receives the element at the appropriate position of the underlying array.
   *
   * @param int $index The index of the array
   * @return mixed
   */
  public function get(int $index) {
    if ($index < 0 || $index >= $this->size())
      throw new InvalidArgumentException("Invalid index. Only a range from 0 to ".($this->size() - 1)." is possible.");
    return $this->array[$index];
  }

  /**
   * Removes all items from this collection.
   *
   * @return Collection
   */
  public function clear(): self {
    $this->array = [];
    return $this;
  }

  /**
   * Checks if the given element is present in this collection.
   *
   * @param $element
   * @return bool
   */
  public function contains($element): bool {
    return $this->indexOf($element) !== null;
  }

  /**
   * Checks if all the given elements are present in this collection.
   *
   * @param Collection|array $elements
   * @return bool
   */
  public function containsAll($elements): bool {
    foreach ($elements as $element)
      if (!$this->contains($element))
        return false;
    return true;
  }

  /**
   * Checks if this collection is empty.
   *
   * @return bool
   */
  public function isEmpty(): bool {
    return $this->size() === 0;
  }

  /**
   * Adds all elements from the source array to this collection.
   *
   * @param Collection|array $elements
   * @return Collection
   */
  public function addAll($elements): self {
    foreach ($elements as $element)
      $this->add($element);
    return $this;
  }

  /**
   * Maps the elements of this collection to a new collection or a plain array.
   *
   * @param callable $callback
   * @param string|null $type If not null, a new collection will be returned
   * @return array|Collection
   */
  public function map(callable $callback, ?string $type = null) {
    $elements = array_map($callback, $this->array);
    if ($type === null) return $elements;

    return (new Collection($type))->addAll($elements);
  }

  /**
   * Calls the given callback on each function of the collection.
   *
   * @param callable $callback
   * @return Collection
   */
  public function each(callable $callback): self {
    array_map($callback, $this->array);
    return $this;
  }

  /**
   * Filters the array based on the callback, see {@link array_filter}
   *
   * @param callable $callback
   * @return Collection
   */
  public function filter(callable $callback): self {
    $this->array = array_filter($this->array, $callback);
    $this->resetKeys();
    return $this;
  }

  /**
   * Sorts the array based on the callback, see {@link usort}
   *
   * @param callable $callback
   * @return Collection
   */
  public function sort(callable $callback): self {
    usort($this->array, $callback);
    $this->resetKeys();
    return $this;
  }

  /**
   * Returns the current count of elements in this collection.
   *
   * @return int
   */
  public function size(): int {
    return sizeof($this->array);
  }

  /**
   * Returns the underlying array of this collection.
   *
   * @return array
   */
  public function toArray(): array {
    return $this->array;
  }

  /**
   * Resets the keys of this collection by discarding the original keys
   */
  private function resetKeys(): void {
    $this->array = array_values($this->array);
  }

  /**
   * Duplicates the collection and returns the new collection.
   *
   * @return Collection An identical copy of the collection.
   */
  public function copy(): Collection {
    return (new Collection($this->type))->addAll($this->array);
  }

  /**
   * <tt>clone $collection</tt>
   */
  public function __clone() {
    return $this->copy();
  }

  /**
   * <tt>echo $collection</tt>
   * @return string
   */
  public function __toString() {
    return "{$this->type}::[".implode(",", $this->array)."]";
  }

  /**
   * <tt>foreach ($collection as $entry) {...}</tt>
   * @return ArrayIterator|\Traversable
   */
  public function getIterator() {
    return new ArrayIterator($this->array);
  }

  /**
   * <tt>json_encode($collection)</tt>
   * @return array|mixed
   */
  public function jsonSerialize() {
    return array_values($this->array);
  }

  /**
   * <tt>serialize($collection)</tt>
   * @return string
   */
  public function serialize() {
    return serialize([$this->type, $this->array]);
  }

  /**
   * <tt>unserialize($collection)</tt>
   * @param string $serialized
   */
  public function unserialize($serialized) {
    list($this->type, $this->array) = unserialize($serialized);
  }

  /**
   * Creates a new collection by using the data supplied in the array and an optional type.
   *
   * @param array $array
   * @param string|null $type
   * @return Collection
   */
  public static function fromArray(array $array, ?string $type = null): self {
    if (sizeof($array) < 1 && $type === null)
      throw new InvalidArgumentException("Array cannot be empty or the type must be supplied.");

    return (new Collection($type ?? Type::get(array_values($array)[0])))->addAll($array);
  }

}