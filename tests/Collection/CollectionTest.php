<?php

namespace BetterOOP\Tests\Collection;


use BetterOOP\Collection\Collection;
use BetterOOP\Helper\Type;
use PHPUnit\Framework\TestCase;
use Throwable;

class CollectionTest extends TestCase
{

  public function testToString(): void {
    $collection = new Collection(Type::STRING);

    $collection->add("some")->add("test");

    $this->assertEquals("string::[some,test]", $collection, "__toString() does not work as expected.");
  }

  public function testAddType(): void {
    $collection = new Collection(Type::STRING);

    $collection->add("test");

    try {
      $collection->add([]);
      $this->assertFalse(true, "add() accepted wrong type.");
    } catch (Throwable $t) {
      $this->assertTrue(true); // suppress risky warning
    }
  }

  public function testSize(): void {
    $collection = new Collection(Type::STRING);

    $this->assertEquals(0, $collection->size(), "size() returned wrong value.");
    $this->assertTrue($collection->isEmpty(), "isEmpty() returned wrong value.");

    $collection->add("some")->add("test");

    $this->assertEquals(2, $collection->size(), "size() returned wrong value.");
    $this->assertFalse($collection->isEmpty(), "isEmpty() returned wrong value.");

    $collection->add("some")->add("test");

    $this->assertEquals(4, $collection->size(), "size() returned wrong value.");
  }

  public function testCopy(): void {
    $collection = new Collection(Type::STRING);

    $collection->add("some")->add("test");

    $otherCollection = $collection->copy();

    $this->assertEquals($otherCollection, $collection, "copy() did not copy correctly.");

    $this->assertNotEquals(spl_object_hash($otherCollection), spl_object_hash($collection), "copy() returned same instance.");
  }

  public function testIndexOf(): void {
    $collection = new Collection(Type::STRING);

    $collection->add("some")->add("test");

    $this->assertEquals(0, $collection->indexOf("some"));

    $this->assertTrue($collection->contains("some"));
    $this->assertFalse($collection->contains("some!!"));

    $this->assertTrue($collection->containsAll(["some", "test"]));
    $this->assertFalse($collection->containsAll(["some!!", "some"]));
  }

  public function testRemove(): void {
    $collection = new Collection(Type::STRING);

    $collection->add("some")->add("test");

    $this->assertEquals(2, $collection->size(), "add() did not work as expected.");

    $collection->remove(0);

    $this->assertEquals(1, $collection->size(), "remove() did not work as expected.");

    $collection->add("some")->add("test");

    $this->assertEquals(3, $collection->size(), "add() did not work as expected.");

    $collection->remove(1, 2);

    $this->assertEquals(1, $collection->size(), "remove() did not work as expected.");
  }

  public function testRemoveElement(): void {
    $collection = new Collection(Type::STRING);

    $collection->add("some")->add("test");

    $this->assertEquals(2, $collection->size(), "add() did not work as expected.");

    $collection->removeElement("some");

    $this->assertEquals(1, $collection->size(), "remove() did not work as expected.");
  }

  public function testGet(): void {
    $collection = new Collection(Type::STRING);

    $collection->add("some")->add("test");

    $this->assertEquals("some", $collection->get(0), "get() did not work as expected.");
    $this->assertEquals(Type::STRING, Type::get($collection->get(0)), "get() returned invalid type.");
  }

  public function testFilter(): void {
    $collection = new Collection(Type::STRING);

    $collection->add("some")->add("test");
    $collection->add("some")->add("test");
    $collection->add("some")->add("test");
    $collection->add("some")->add("test");

    $this->assertEquals(8, $collection->size(), "add() did not work as expected.");

    $collection->filter(function ($a) {
      return strpos($a, "t") !== 0;
    });

    $this->assertEquals(4, $collection->size(), "filter() did not work as expected.");
    $this->assertEquals("some", $collection->get(0), "filter() did not work as expected.");
  }

  public function testSort(): void {
    $collection = new Collection(Type::STRING);

    $collection->add("some")->add("test");
    $collection->add("some")->add("test");
    $collection->add("some")->add("test");
    $collection->add("some")->add("test");

    $this->assertEquals(8, $collection->size(), "add() did not work as expected.");

    $collection->sort(function ($a, $b) {
      return min(1, max(-1, strcmp($a, $b)));
    });

    $this->assertEquals("some", $collection->get(0), "filter() did not work as expected.");
    $this->assertEquals("some", $collection->get(1), "filter() did not work as expected.");
    $this->assertEquals("test", $collection->get(4), "filter() did not work as expected.");
    $this->assertEquals("test", $collection->get(5), "filter() did not work as expected.");
  }

  public function testCollectionIterator(): void {
    $collection = new Collection(Type::STRING);

    $collection->add("some")->add("some");
    $collection->add("some")->add("some");
    $collection->add("some")->add("some");
    $collection->add("some")->add("some");

    $count = 0;
    foreach ($collection as $entry) {
      $this->assertEquals("some", $entry, "Iterator failed.");
      $count++;
    }

    $this->assertEquals(8, $count, "Iterator failed.");
  }

  public function testMap(): void {
    $collection = new Collection(Type::STRING);

    $collection->add("some")->add("teeest");

    $array = $collection->map(function ($a) {
      return strlen($a);
    });

    $mappedCollection = $collection->map(function ($a) {
      return strlen($a);
    }, Type::INTEGER);

    $this->assertEquals(Type::ARRAY, Type::get($array), "map() returned invalid type.");
    $this->assertEquals(Collection::class, Type::get($mappedCollection), "map() returned invalid type.");

    $this->assertEquals(2, sizeof($array), "map() returned wrong size.");
    $this->assertEquals(2, $mappedCollection->size(), "map() returned wrong size.");

    $this->assertEquals(4, $array[0], "map() failed.");
    $this->assertEquals(4, $mappedCollection->get(0), "map() failed.");

    $this->assertEquals(6, $array[1], "map() failed.");
    $this->assertEquals(6, $mappedCollection->get(1), "map() failed.");
  }

  public function testEach(): void {
    $collection = new Collection(Type::STRING);

    $collection->add("some")->add("teeest");

    $count = 0;
    $collection->each(function ($a) use (&$count) {
      $count += strlen($a);
    });

    $this->assertEquals(10, $count, "each() failed.");
  }

  public function testSerialize(): void {
    $collection = new Collection(Type::STRING);

    $collection->add("some")->add("test");

    $this->assertEquals("[\"some\",\"test\"]", json_encode($collection), "Json serialize failed.");

    $otherCollection = unserialize(serialize($collection));

    $this->assertNotEquals(spl_object_hash($otherCollection), spl_object_hash($collection), "Serialize/unserialize did not clone.");
  }

  public function testArrayMethods(): void {
    try {
      Collection::fromArray([]);
      $this->assertFalse(true, "fromArray() accepted empty array without type.");
    } catch (Throwable $t) {
      $this->assertTrue(true); // suppress risky warning
    }

    try {
      Collection::fromArray([], Type::STRING);
      $this->assertTrue(true); // suppress risky warning
    } catch (Throwable $t) {
      $this->assertFalse(true, "fromArray() did not accept empty array with given type.");
    }

    try {
      Collection::fromArray(["this" => "is a test"]);
      $this->assertTrue(true); // suppress risky warning
    } catch (Throwable $t) {
      $this->assertFalse(true, "fromArray() failed auto guessing with key=>value array.");
    }

    try {
      Collection::fromArray(["this" => "is a test"], Type::INTEGER);
      $this->assertFalse(true, "fromArray() wrote wrong type into collection.");
    } catch (Throwable $t) {
      $this->assertTrue(true); // suppress risky warning
    }

    $collection = new Collection(Type::STRING);

    $collection->add("some")->add("test");

    $this->assertEquals(2, sizeof($collection->toArray()), "toArray() failed.");
  }
}