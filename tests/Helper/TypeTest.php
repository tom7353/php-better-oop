<?php

namespace BetterOOP\Tests\Helper;


use BetterOOP\Helper\Type;
use PHPUnit\Framework\TestCase;

class TypeTest extends TestCase {

  public function testGet(): void {

    $this->assertEquals(Type::STRING, Type::get("test"));

    $this->assertEquals(Type::INTEGER, Type::get(1));

    $this->assertEquals(Type::FLOAT, Type::get(1.2));
    $this->assertEquals(Type::DOUBLE, Type::get(1.2));

    $this->assertEquals(Type::ARRAY, Type::get([]));

    $this->assertEquals(Type::BOOLEAN, Type::get(true));

    $this->assertEquals(Type::NULL, Type::get(null));

    $this->assertEquals(Type::class, Type::get(new Type()));

  }

}