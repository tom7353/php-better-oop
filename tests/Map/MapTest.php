<?php

namespace BetterOOP\Tests\Map;


use BetterOOP\Helper\Type;
use BetterOOP\Map\Map;
use BetterOOP\String\CharSequence;
use PHPUnit\Framework\TestCase;
use Throwable;

class MapTest extends TestCase {

  public function testToString(): void {
    $map = new Map(Type::STRING, Type::STRING);

    $map->put("some", "test");
    $map->put("test", "some");

    $this->assertEquals("string=>string::[some=>test,test=>some]", $map, "__toString() does not work as expected.");
  }

  public function testAddType(): void {
    $map = new Map(Type::STRING, Type::STRING);

    $map->put("test", "test");

    try {
      $map->put(1, 2);
      $map->put("asd", 2);
      $this->assertFalse(true, "put() accepted wrong type.");
    } catch (Throwable $t) {
      $this->assertTrue(true); // suppress risky warning
    }
  }

  public function testSize(): void {
    $map = new Map(Type::STRING, Type::STRING);

    $this->assertEquals(0, $map->size(), "size() returned wrong value.");
    $this->assertTrue($map->isEmpty(), "isEmpty() returned wrong value.");

    $map->put("some", "test");

    $this->assertEquals(1, $map->size(), "size() returned wrong value.");
    $this->assertFalse($map->isEmpty(), "isEmpty() returned wrong value.");

    $this->assertNotNull($map->put("some", "test"), "put() returned wrong previous value.");

    $this->assertEquals(1, $map->size(), "size() returned wrong value.");

    $map->put("some_cool", "test");

    $this->assertEquals(2, $map->size(), "size() returned wrong value.");
  }

  public function testCopy(): void {
    $map = new Map(Type::STRING, Type::STRING);

    $map->put("some", "test");

    $otherMap = $map->copy();

    $this->assertEquals($otherMap, $map, "copy() did not copy correctly.");

    $this->assertNotEquals(spl_object_hash($otherMap), spl_object_hash($map), "copy() returned same instance.");
  }

  public function testIndexOf(): void {
    $map = new Map(Type::STRING, Type::STRING);

    $map->put("some", "test");

    $this->assertNotNull($map->get("some"));

    $this->assertTrue($map->containsKey("some"));
    $this->assertFalse($map->containsKey("some!!"));

    $this->assertTrue($map->containsValue("test"));
    $this->assertFalse($map->containsValue("asd"));
  }

  public function testRemove(): void {
    $map = new Map(Type::STRING, Type::STRING);

    $map->put("some", "test");

    $this->assertEquals(1, $map->size(), "put() did not work as expected.");

    $map->removeKey("some1");

    $this->assertEquals(1, $map->size(), "removeKey() did not work as expected.");

    $map->removeKey("some");

    $this->assertEquals(0, $map->size(), "removeKey() did not work as expected.");
  }

  public function testGet(): void {
    $map = new Map(Type::STRING, Type::STRING);

    $map->put("some", "test");

    $this->assertEquals("test", $map->get("some"), "get() did not work as expected.");
    $this->assertEquals(Type::STRING, Type::get($map->get("some")), "get() returned invalid type.");
  }

  public function testMapIterator(): void {
    $map = new Map(Type::STRING, Type::STRING);

    $map->put("some1", "test");
    $map->put("some2", "test");
    $map->put("some3", "test");
    $map->put("some4", "test");

    $count = 0;
    foreach ($map as $key => $value) {
      $this->assertTrue((new CharSequence($key))->startsWith("some"), "Iterator failed.");
      $this->assertEquals("test", $value, "Iterator failed.");
      $count++;
    }

    $this->assertEquals(4, $count, "Iterator failed.");
  }

  public function testSerialize(): void {
    $map = new Map(Type::STRING, Type::STRING);

    $map->put("some", "test");

    $this->assertEquals("{\"some\":\"test\"}", json_encode($map), "Json serialize failed.");

    $otherMap = unserialize(serialize($map));

    $this->assertNotEquals(spl_object_hash($otherMap), spl_object_hash($map), "Serialize/unserialize did not clone.");
  }

}