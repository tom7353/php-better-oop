<?php

namespace BetterOOP\Tests\String;


use BetterOOP\String\CharSequence;
use PHPUnit\Framework\TestCase;

class CharSequenceTest extends TestCase {

  public function testCharSequence(): void {
    $sequence = new CharSequence("hello world");

    $this->assertEquals(11, $sequence->length(), "length() failed");

    $this->assertEquals(12, $sequence->append("!")->length(), "append() failed");

    $this->assertEquals("HELLO WORLD!", $sequence->toUpperCase(), "toUpperCase() failed");

    $this->assertTrue($sequence->matches('/^[A-Z !]+$/'), "matches() failed");
    $this->assertFalse($sequence->matches('/^[A-Z!]+$/'), "matches() failed");
    $this->assertTrue($sequence->matches('/^[a-z !]+$/i'), "matches() failed");

    $this->assertEquals("Oh, HELLO WORLD!", $sequence->prepend("Oh, "), "prepend() failed");

    $this->assertEquals("oh, hello world!", $sequence->toLowerCase(), "toLowerCase() failed");

    $this->assertEquals("h", $sequence->charAt(1), "charAt() failed");

    $this->assertEquals("oh, hey world!", $sequence->replace("hello", "hey"), "replace() failed");

    $this->assertTrue($sequence->equalsIgnoreCase("oh, HEY world!"), "equalsIgnoreCase() failed");
    $this->assertTrue($sequence->equals("oh, hey world!"), "equals() failed");

    $this->assertFalse($sequence->equalsIgnoreCase("oh, heey world!"), "equalsIgnoreCase() failed");
    $this->assertFalse($sequence->equals("oh, Hey world!"), "equals() failed");

    $this->assertTrue($sequence->endsWith("world!"), "endsWith() failed");
    $this->assertTrue($sequence->startsWith("oh,"), "startsWith() failed");

    $this->assertFalse($sequence->endsWith("world!!"), "endsWith() failed");
    $this->assertFalse($sequence->startsWith(" oh,"), "startsWith() failed");

    $this->assertEquals(3, sizeof($sequence->split(" ")), "split() failed");
    $this->assertEquals(2, sizeof($sequence->split(" ", 2)), "split() failed");

    $this->assertNotEquals(spl_object_hash($sequence), spl_object_hash($sequence->substring(3)), "substring() did not copy");

    $this->assertEquals("world!", $sequence->substring(8), "substring() failed");
    $this->assertEquals("wo", $sequence->substring(8, 9), "substring() failed");
    $this->assertEquals("w", $sequence->substring(8, 8), "substring() failed");

    $this->assertEquals("hey world!", $sequence->cut(4), "substring() failed");

    $this->assertEquals("hey world!  ", $sequence->append("  "), "append() failed");
    $this->assertEquals("hey world!", $sequence->trim(), "trim() failed");

    $this->assertNotEquals(spl_object_hash($sequence), spl_object_hash($sequence->copy()), "copy() did not copy");

    $this->assertEquals("AA", CharSequence::fromAsciiArray([65, 65]), "fromAsciiArray() failed");
    $this->assertEquals("AB", CharSequence::fromArray(["A", "B"]), "fromAsciiArray() failed");

    foreach ($sequence as $char)
      $this->assertEquals(1, mb_strlen($char), "Iterator failed.");

    $this->assertEquals("\"hey world!\"", json_encode($sequence), "Json serialize failed.");

    $otherSequence = unserialize(serialize($sequence));

    $this->assertNotEquals(spl_object_hash($otherSequence), spl_object_hash($sequence), "Serialize/unserialize did not clone.");

    $sequence = new CharSequence("This is a cool string!");
    $this->assertEquals("!", $sequence->replaceRegEx('/[a-z ]/mi', ""), "replaceRegEx() failed");

    $sequence = new CharSequence("This is a cool string!");
    $this->assertEquals("!", $sequence->replaceRegEx('/^[^!]+/mi', ""), "replaceRegEx() failed");
  }
}